# Webapp Template

**REMEMBER to REMOVE or EDIT this file before submitting!**

## For pom.xml edit:
 - AUTHOR: author name
 - PACKAGE: package name, eg. it.unipd.dei.webapp
 - ARTIFACT-ID: tomcat package, must be unique, use something like appName_yourName
 - PROJECT-NAME: name and brief description of the project
 - PROJECT-DESCRIPTION: description of the project
 - REPOSITORY: URL of the repository on bitbucket
 - OPTIONAL: these mark the dependencies, remove the unused ones.

 ## For context.xml edit:
  - AUTHOR
  - ARTIFACT-ID

The database credential should be already correct.

## For web.xml edit:
 - AUTHOR
 - ARTIFACT-ID
 - PROJECT-NAME
 - PROJECT-DESCRIPTION
 - LANDING-PAGE: A landing page, remember to specify the path if required! Also needs the extension of the file. If this element is not specified tomcat will try to find a file named "index.html" in the root.
 - SERVLET-FILENAME: Name of the java file of the servlet, without .java
 - PACKAGE: watch out, remember to add the "servlet" directory if you use it.
 - RESOURCE-DESCRIPTION: Brief description of the resource.

 If multiple servlets are used remember to check the **url-pattern**!

